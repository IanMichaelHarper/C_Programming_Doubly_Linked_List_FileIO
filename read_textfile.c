#include <stdio.h>
#include <stdlib.h>

#include "structs.h"

List read_textfile(char * filename)
{
	List l;
	l.head = NULL;
	l.tail = NULL;

	char word[256];

	FILE * f;

	f = fopen(filename, "r");
	if (f == NULL)
	{
		printf("'%s' didn't open\n", filename);
		return l;
	}
	
	while (fscanf(f, "%s", word) != EOF)
	{
		l = append(word, l);
	}

	fclose(f);
	return l;
}
