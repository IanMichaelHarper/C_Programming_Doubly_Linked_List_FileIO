#include <stdio.h>
#include <stdlib.h>

#include "structs.h"

List reverse(List l)
{
	//start at bottom of list
	if (l.tail -> next == NULL)
	{
		Cell * current_cell = l.tail;
		Cell * temp;

		while (current_cell ->prev != NULL)
		{
			//swap pointers 
			temp = current_cell -> prev;
			current_cell -> prev = current_cell -> next;
			current_cell -> next = temp;
			
			//iterate up through list since current_cell on RHS is old current cell
			//current_cell on LHS is new current cell
			current_cell = current_cell->next;  
		}
		
		//swap pointers for last one
		temp = current_cell -> prev;
		current_cell -> prev = current_cell -> next;
		current_cell -> next = temp;
		
		//ensure head and tail are swapped
		l.head = l.tail;
		l.tail = current_cell;
	}
	return l;
}

