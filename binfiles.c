#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "structs.h"

void write_binfile(List l, char * filename)
{
	FILE * f;
	f = fopen(filename, "wb");
	if (f == NULL)
	{
		printf("'%s' didn't open\n", filename);
	}
	
	//start at top of list
	if (l.head->prev == NULL)
	{
		Cell * current_cell = l.head;
		while (current_cell != NULL)
		{	
			int sizeOfString = strlen(current_cell -> word) + 1;  //find length of word to be written, including \0 char
			fwrite(&sizeOfString, sizeof(int), 1, f);  //write length of word
			fwrite(current_cell->word, sizeof(char), sizeOfString, f);  //write word
			current_cell = current_cell -> next;  //iterate to next cell
		}
	}
	fclose(f);
}

List read_binfile(List l, char * filename)
{
	l.head = NULL;
	l.tail = NULL;

	FILE * f;
	f = fopen(filename, "rb");
	if (f == NULL)
	{
		printf("'%s' didn't open\n", filename);
	}

	char word[256];
	int strlen;
	while (fread(&strlen, sizeof(int), 1, f) != 0)  //read until EOF
	{
		fread(word, sizeof(char), strlen, f);
		l = append(word, l);
	}
	
	fclose(f);
	return l;
}
