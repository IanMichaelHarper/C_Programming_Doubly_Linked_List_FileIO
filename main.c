#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "structs.h"
		
int main()
{
	//initalise list
	List l;
	l.head = NULL;
	l.tail = NULL;

	//append new cells with new words to list
	l = append( "apple", l);
	l = append( "pear", l);
	l = append( "plum", l);
	l = append( "orange", l);
	print_list(l);
	printf("\n");

	//insert banana before plum
	l = insert_before("plum", "banana", l);
	print_list(l);
	printf("\n");

	//insert kiwi after pear
	l = insert_after("pear", "kiwi", l);
	print_list(l);
	printf("\n");

	//test insert functions for words that are not in list
	l = insert_before("peach", "James",l);
	l = insert_after("giant", "peach", l);
	print_list(l);

	//reverse list
	printf("\n");
	l = reverse(l);
	print_list(l);

	printf("\n");

	//create new list from txt file
	List coins = read_textfile("words.txt");  
	print_list(coins); 
	printf("\n");
	
	write_binfile(coins, "coins.txt");

	//create new list from binary txt file
	List coinsbin;
	coinsbin = read_binfile(coinsbin, "coins.txt");
	print_list(coinsbin);

	free_list(l);
	free_list(coins);
	free_list(coinsbin);

	return 0;
}
