OBJECTS = append.o \
	print_and_free.o \
	inserts.o \
	reverse.o \
	read_textfile.o \
	binfiles.o \
	main.o 

main: $(OBJECTS) structs.h
	gcc -g -Wall -o main $(OBJECTS)

main.o: main.c structs.h
	gcc -g -c main.c

append.o: append.c structs.h
	gcc -g -c append.c

print_and_free.o: print_and_free.c structs.h
	gcc -c print_and_free.c

inserts.o: inserts.c structs.h
	gcc -c inserts.c

reverse.o: reverse.c structs.h
	gcc -c reverse.c

read_textfile.o: read_textfile.c structs.h
	gcc -g -c read_textfile.c 

binfiles.o: binfiles.c structs.h
	gcc -g -c binfiles.c

clean: 
	rm *.o main 
